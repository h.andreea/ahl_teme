N = int(input ("Scrie un numar N: "))
lista_N = list(range(N))
dict_N = {}

def isPrim(number):
    div = 0
    for x in range(1, N+1):
        if number % x == 0:
            div = div+1
    return div == 2

def makeSum(number):
    suma = number * (number+1) / 2
    return suma
               

if 0 <=  N :
    for element in lista_N:
        dict_N[element] = [isPrim(element), makeSum(element)]
print(dict_N)


