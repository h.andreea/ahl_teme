d1 = {'k1':1, 'k2': 2, 'k3': 3}
d2 = {'k1':2, 'k3': 6, 'k4' : 8, 'k5': 10}

for key, value in d2.items():
    if key not in d1:
        d1[key] = value
              
print(d1)