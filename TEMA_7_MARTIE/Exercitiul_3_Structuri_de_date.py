id_utilizator = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6']
deschidere_usa = [2, 4, 6, 5, 2, 4] #nr. deschideri/inchideri usa pt. fiecare cercetator

for deschideri in deschidere_usa:
    if deschideri % 2 != 0:
        pozitie = deschidere_usa.index(deschideri)
        cercetator = id_utilizator[pozitie]
        print('Cercetătorul ce stă peste program este:', cercetator)