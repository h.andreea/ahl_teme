lista = [1, 2, 3, 4, 1, 3, 5]

#Prima metoda:
lista_noua = []
for element in lista:
    if element in lista_noua:
        print("Elementul %s este dublat." % element)
    if element not in lista_noua:
        lista_noua.insert(0, element)
print(lista_noua, "Lista elementelor fara dubluri.")

        
#A -II-a metoda:
for element in lista:
    if lista.count(element) >= 2:
        print("Exista dubluri in lista.")
        break
    else:
        print("Nu exista dubluri in lista.")
        break

#A -III - a metoda: